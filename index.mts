/**
 * SPDX-PackageName: kwaeri/mysql-database-driver
 * SPDX-PackageVersion: 0.6.1
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


// INCLUDES
//import { ServiceProvider } from './src/service.mjs';


// ESM WRAPPER
export {
    MySQLDriver
} from './src/mysql-database-driver.mjs';

// DEFAULT EXPORT
//export default ServiceProvider;

