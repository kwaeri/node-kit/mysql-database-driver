/**
 * SPDX-PackageName: kwaeri/mysql-database-driver
 * SPDX-PackageVersion: 0.6.1
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


export default {
    "type": "mysql",
    "host": "localhost",
    "port": 3306,
    "database": "nodekit_test",
    "user": "nodekit_test",
    "password": "TestPass777"
}

export const test = {
    "type": "mysql",
    "host": "mysql",
    "port": 3306,
    "database": "nodekit_test",
    "user": "root",
    "password": "TestPass777"
}