/**
 * SPDX-PackageName: kwaeri/mysql-database-driver
 * SPDX-PackageVersion: 0.6.1
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'

// INCLUDES
import * as assert from 'assert';
import { MySQLDriver } from '../src/mysql-database-driver.mjs';
import { default as myDefaultConfig, test } from '../data/db-config-mysql.mjs';
import debug from 'debug';


// DEFINES
const DEBUG = debug( 'nodekit:mysql-database-driver-test' );

let MIGRATION_ENV = process.env.NODE_ENV || "default";

const conf = {
    default: myDefaultConfig,
    test: test
};

const mydbo = new MySQLDriver( ( conf as any )[MIGRATION_ENV] );


// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {
        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );
    }
);


describe(
    'MySQLDriver Functionality Test Suite',
    () => {

        describe(
            'Create Table Test',
            () => {
                it(
                    'Should create a table named `people` in the `nodekit_test` MySQL database, and return a server status of 2.',
                    async () => {
                        let results = null;

                        try {
                            // Get some results from the migrator:
                            results = await mydbo
                            .query( `create table if not exists people ` +
                                    `( id int(11) not null auto_increment,` +
                                    `first_name varchar(255), ` +
                                    `last_name varchar(255), ` +
                                    `age int(11), ` +
                                    `primary key (id) );` );

                            DEBUG( `Server Status: ${( results.rows as any ).serverStatus}` );

                            // Assert that the comparison is equivalent to 'true':
                            return Promise.resolve(
                                assert.equal(
                                    true,
                                    ( ( results.rows as any ).serverStatus == 2 )
                                )
                            );
                        }
                        catch( error ) {
                            DEBUG( `[ERROR]: ${error}` );

                            return Promise.reject( Error( `${error}` ) );
                        }
                    }
                );
            }
        );


        describe(
            'Insert Record Test',
            () => {
                it(
                    'Should insert a record into the `people` table, returning the # of affected rows and insert id of the created record.',
                    async () => {
                        let results = null;

                        try {
                            // Get some results from the migrator:
                            results = await mydbo
                            .query( `insert into people ( first_name, last_name, age ) values('Richard', 'Winters', 32);` );

                            DEBUG( `Affected Rows: ${(results.rows as any ).affectedRows}, Insert Id: ${(results.rows as any ).insertId}` );

                            // Assert that the comparison is equivalent to 'true':
                            return Promise.resolve(
                                assert.equal(
                                    true,
                                    (
                                        (results.rows as any ).affectedRows == 1 &&
                                        (results.rows as any ).insertId     == 1
                                    ) ? true : false
                                )
                            );
                        }
                        catch( error ) {
                            DEBUG( `[ERROR]: ${error}` );

                            return Promise.reject( Error( `${error}` ) );
                        }
                    }
                );
            }
        );


        describe(
            'Insert Record [Prepared Statement] Test',
            () => {
                it(
                    'Should insert a record into the `people` table, returning the # of affected rows and insert id of the created record.',
                    async () => {
                        let results = null;

                        try {
                            // Get some results from the migrator:
                            results = await mydbo
                            .query( `insert into people ( first_name, last_name, age ) values(?, ?, ?);`, ["Richard", "Winters", 32] );

                            DEBUG( `Affected Rows: ${(results.rows as any ).affectedRows}, Insert Id: ${(results.rows as any ).insertId}` );

                            // Assert that the comparison is equivalent to 'true':
                            return Promise.resolve(
                                assert.equal(
                                    true,
                                    (
                                        (results.rows as any ).affectedRows == 1 &&
                                        (results.rows as any ).insertId     == 2
                                    ) ? true : false
                                )
                            );
                        }
                        catch( error ) {
                            DEBUG( `[ERROR]: ${error}` );

                            return Promise.reject( Error( `${error}` ) );
                        }
                    }
                );
            }
        );


        describe(
            'Select Record Test',
            () => {
                it(
                    'Should return the column values of the first record inserted into the `people` table.',
                    async () => {
                        let results = null;

                        try {
                            // Get some results from the migrator:
                            results = await mydbo
                            .query( `select * from people where id=1 and first_name='Richard' and last_name='Winters' and age=32;` );

                            DEBUG( `Returned record with Id: ${(results.rows as any)[0].id}` +
                                         `, First name: ${(results.rows as any)[0].first_name}` +
                                         `, Last name: ${(results.rows as any)[0].last_name}` +
                                         `, Age: ${(results.rows as any)[0].age}` );

                            // Assert that the comparison is equivalent to 'true':
                            return Promise.resolve(
                                assert.equal(
                                    true,
                                    (
                                        (results.rows as any)[0].id           == 1           &&
                                        (results.rows as any)[0].first_name   == 'Richard'   &&
                                        (results.rows as any)[0].last_name    == 'Winters'   &&
                                        (results.rows as any)[0].age          == 32
                                    ) ? true : false
                                )
                            );
                        }
                        catch( error ) {
                            DEBUG( `[ERROR]: ${error}` );

                            return Promise.reject( Error( `${error}` ) );
                        }
                    }
                );
            }
        );


        describe(
            'Delete Record Test',
            () => {
                it(
                    'Should delete the record(s) previously inserted into the `people` table and return the # of affected rows (2).',
                    async () => {
                        let results = null;

                        try {
                            // Get some results from the migrator:
                            results = await mydbo
                            .query( `delete from people where first_name='Richard' and last_name='Winters' and age=32;` );

                            DEBUG( `Affected Rows: ${(results.rows as any ).affectedRows}` );

                            // Assert that the comparison is equivalent to 'true':
                            return Promise.resolve(
                                assert.equal(
                                    true,
                                    ( (results.rows as any ).affectedRows == 2 ) ? true : false
                                )
                            );
                        }
                        catch( error ) {
                            DEBUG( `[ERROR]: ${error}` );

                            return Promise.reject( Error( `${error}` ) );
                        }
                    }
                );
            }
        );


        describe(
            'Drop Table Test',
            () => {
                it(
                    'Should drop the `people` table in the `nodekit_test` MySQL database and return the server status.',
                    async () => {
                        let results = null;

                        try {
                            // Get some results from the migrator:
                            results = await mydbo
                            .query( `drop table people;` );

                            DEBUG( `Server Status: ${(results.rows as any ).serverStatus}` );

                            // Assert that the comparison is equivalent to 'true':
                            return Promise.resolve(
                                assert.equal(
                                    true,
                                    ( (results.rows as any ).serverStatus == 2 ) ? true : false
                                )
                            );
                        }
                        catch( error ) {
                            DEBUG( `[ERROR]: ${error}` );

                            return Promise.reject( Error( `${error}` ) );
                        }
                    }
                );
            }
        );

    }
);

